@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h4>Список товаров</h4>
        </div>
        <div class="row">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr class="text-center">
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Цена</th>
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1 ?>
                @foreach($products as $product)
                    <tr>
                        <td>{{$counter}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->description}} </td>
                        <td>{{$product->price}}</td>
                    </tr>
                    <?php $counter++ ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
